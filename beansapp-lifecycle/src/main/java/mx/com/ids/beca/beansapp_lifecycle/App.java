package mx.com.ids.beca.beansapp_lifecycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import mx.com.ids.beca.beansapp_lifecycle.scope.Speaker;
import mx.com.ids.beca.beansapp_lifecycle.scope.SpeakerTwo;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {	
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
    	System.out.println("");
    	
    	// --------------- Singleton ---------------
    	SpeakerTwo speakerSingletonOne = (SpeakerTwo) appContext.getBean("mySingleton");
    	speakerSingletonOne.setMessage("Setted message on SingletonOne!!");
    	System.out.println(speakerSingletonOne.getMessage());
    	
    	SpeakerTwo speakerSingletonTwo = (SpeakerTwo) appContext.getBean("mySingleton");
    	System.out.println(speakerSingletonTwo.getMessage());
    	// -----------------------------------------
    	
    	System.out.println("");
    	((ClassPathXmlApplicationContext) appContext).close();
    	
    	System.out.println("");
    	
    	appContext = new ClassPathXmlApplicationContext("beans.xml");
    	System.out.println("");
    	
    	// --------------- Prototype ---------------
    	Speaker speakerPrototypeOne = (Speaker) appContext.getBean("myPrototype");
    	speakerPrototypeOne.setMessage("Setted message on PrototypeOne!!");
    	System.out.println(speakerPrototypeOne.getMessage());
    	
    	Speaker speakerPrototypeTwo = (Speaker) appContext.getBean("myPrototype");
    	System.out.println(speakerPrototypeTwo.getMessage());
    	// -----------------------------------------
    	
    	System.out.println("");
    	((ClassPathXmlApplicationContext) appContext).close();
    	
    	System.out.println("");
    	
    	appContext = new ClassPathXmlApplicationContext("beans.xml");
    	System.out.println("");
    	
    	// --------------- Prototype w/XML ---------------
    	Speaker speakerPrototypeTwoA = (Speaker) appContext.getBean("prototypeTwo");
    	speakerPrototypeTwoA.setMessage("Setted message on PrototypeTwoA");
    	System.out.println(speakerPrototypeTwoA.getMessage());
    	
    	Speaker speakerPrototypeTwoB = (Speaker) appContext.getBean("prototypeTwo");
    	System.out.println(speakerPrototypeTwoB.getMessage());
    	// -----------------------------------------

    	System.out.println("");
    	((ClassPathXmlApplicationContext) appContext).close();
    	
    }
}
