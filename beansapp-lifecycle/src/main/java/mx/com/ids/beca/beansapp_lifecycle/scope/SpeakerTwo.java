package mx.com.ids.beca.beansapp_lifecycle.scope;

public class SpeakerTwo{
	
	private String MESSAGE;
	
	public SpeakerTwo speaker() {
		return new SpeakerTwo();
	}

	public String getMessage() {
		return MESSAGE;
	}

	public void setMessage(String message) {
		MESSAGE = message;
	}
	
	public void init() throws Exception {
		System.out.println("Iniciando desde XML...");
	}
	
	public void destroy() throws Exception {
		System.out.println("El bean se destruirá desde XML...");
	}
	
}
