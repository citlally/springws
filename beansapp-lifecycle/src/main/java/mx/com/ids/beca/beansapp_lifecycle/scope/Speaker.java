package mx.com.ids.beca.beansapp_lifecycle.scope;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Speaker implements InitializingBean, DisposableBean{
	
	private String MESSAGE;
	
	public Speaker speaker() {
		return new Speaker();
	}

	public String getMessage() {
		return MESSAGE;
	}

	public void setMessage(String message) {
		MESSAGE = message;
	}
	
	public void afterPropertiesSet() throws Exception {
		System.out.println("Iniciando desde InitializingBean...");
		
	}

	public void destroy() throws Exception {
		System.out.println("El bean se destruirá desde DisposableBean...");
		
	}

}
