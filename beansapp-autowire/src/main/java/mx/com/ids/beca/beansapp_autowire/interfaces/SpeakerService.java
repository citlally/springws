package mx.com.ids.beca.beansapp_autowire.interfaces;

public interface SpeakerService {
	String speak();
}
