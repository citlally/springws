package mx.com.ids.beca.beansapp_autowire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//import mx.com.ids.beca.beansapp_autowire.interfaces.SpeakerService;
import mx.com.ids.beca.beansapp_autowire.interfaces.TalkService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // Accedemos al beans.xml
    	// Abrimos el contexto
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
    	
    	// ---------- REFERENCIA DE BEANS ----------
    	// Accedemos por Class
    	TalkService talkService = appContext.getBean("talkService", TalkService.class);
    	talkService.speakWords("Hitler"); // Le pasamos el parametro
    	
    	// Inyección a bean con autowire-candidate="false" pero con referencia explicita
    	TalkService talkServiceSpanish = appContext.getBean("talkServiceSpanish", TalkService.class);
    	talkServiceSpanish.speakWords("Benito"); // Le pasamos el parametro
    	
    	TalkService talkServiceEnglish = appContext.getBean("talkServiceEnglish", TalkService.class);
    	talkServiceEnglish.speakWords("Jhon"); // Le pasamos el parametro
    	
    	
    	// Se cierra el contexto
    	((ClassPathXmlApplicationContext) appContext).close();
    }
}