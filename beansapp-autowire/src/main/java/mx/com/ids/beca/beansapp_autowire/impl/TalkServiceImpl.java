package mx.com.ids.beca.beansapp_autowire.impl;

import mx.com.ids.beca.beansapp_autowire.interfaces.SpeakerService;
import mx.com.ids.beca.beansapp_autowire.interfaces.TalkService;

public class TalkServiceImpl implements TalkService{
	
	// ---- CLASS PARA REFERENCIA DE BEANS ----
	
	private SpeakerService speakerService;
	
	public void speakWords(final String name) {
		System.out.println("Speaker: " + name);
		System.out.println("says: " + speakerService.speak() );
	}

	public void setSpeakerService(SpeakerService speakerService) {
		this.speakerService = speakerService;
	}
	
}
