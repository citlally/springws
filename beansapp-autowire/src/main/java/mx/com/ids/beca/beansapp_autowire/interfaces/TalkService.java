package mx.com.ids.beca.beansapp_autowire.interfaces;

public interface TalkService {
	
	public void speakWords(final String words);

}
