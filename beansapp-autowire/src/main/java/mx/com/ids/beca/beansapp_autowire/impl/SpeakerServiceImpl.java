package mx.com.ids.beca.beansapp_autowire.impl;

import mx.com.ids.beca.beansapp_autowire.interfaces.SpeakerService;

public class SpeakerServiceImpl implements SpeakerService {
	
	// ---- CLASS PARA CONSTRUCTOR INJECTION ----
	
	// Las propiedades deben ser privadas
	private String dialog;
	
	//Constructor Injection
	public SpeakerServiceImpl(final String dialog) {
		this.dialog = dialog;
	}
	
	public SpeakerServiceImpl() {
		
	}
	
	// Las propiedades deben tener Setters y/o Getters públicos
	public void setDialog(String dialog) {
		this.dialog = dialog;
	}

	// Método desde Interface
	public String speak() {
		return dialog;
	}

}
