
Repositorio ws para proyectos dentro del curso Spring.
	
	- AppWebDemo:
		"Uso de servidor Tomcat en Spring para desplegar Servlets."
	
	- helloworld:
		"Creación de un servicio y su implementación, 
		también creamos una prueba unitaria para el servicio,
		y un bean. Proyecto con Maven."

		"Se implemento el constructor injection con un bean, y posteriormente se modifico para 
		que el mismo bean implemente Method Injection."
	
	- AppDesignPatterns:
		"Proyecto con aplicación de patrones de diseño."
		<Hay un archivo txt con Ejercicio-01>

		> mx.com.ids.beca.factory
			"Patron de diseño Factory aplicado a un ejemplo con creación de Malteadas."

		> mx.com.ids.beca.proxy
			"Patron de diseño Proxy aplicado a un ejemplo simulación de generador de Mapas."

		> mx.com.ids.beca.prototype
			"Patron de diseño Prototype aplicado a un ejemplo de creación de Credenciales."

		> mx.com.ids.beca.dtovo
			"Patron de diseño DTO/VO aplicado a un ejemplo de administrador con empleados."

	- appinjection:
		"Creación de un Talkservice para probar Constructor Injection, Method Injection y Beans de referencia."

	- beansapp-lifecycle:
		"Inciso b del Ejercicio 3, se creo una clase Speaker para utilizar beans con scope: singleton y prototype,
		también se implementó InitializingBean, DisposableBean e init y destroy en XML para el cyclo de vida."

	- beansapp-autowire:
		"Inciso c del Ejercicio 3, se creo un proyecto para la ejemplificación de los tipos de autowire."
		

<<< EN LA RAMA "annotations" EXISTE (con anotaciones):

	- appinjection
	- beansapp-lifecycle
	- beansapp-autowire
	
	- helloworld2-springdata:
		"Proyecto ejemplo de implementación de spring data."

	- appdemo:
		"Proyecto de spring data, cuenta con 4 entidades, sus respectivos servicios de negocio y sus asociaciones 
		utilizando Spring Data JPA"

	- xspringmvcdemo:
		"proyecto de Spring MVC configurado mediante XML"

	- xspringmvcdemo02:
		"proyecto de Spring MVC configurado con annotaciones"
