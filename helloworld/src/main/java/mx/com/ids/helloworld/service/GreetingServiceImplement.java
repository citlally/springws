package mx.com.ids.helloworld.service;

public class GreetingServiceImplement implements GreetingService {
	
	private String msg;
	
	/* 
	 * Constructor Injection
	 * 
	public GreetingServiceImplement(final String msg) {
		this.msg = msg;
	}
	*/
	
	public GreetingServiceImplement() {
	}

	@Override
	public String saludar() {
		return msg;
	}
	
	/**
	 * @param msg el mensaje es asignado
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String getMsg() {
		return this.msg;
	}

}
