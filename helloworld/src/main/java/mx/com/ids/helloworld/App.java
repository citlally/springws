package mx.com.ids.helloworld;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import mx.com.ids.helloworld.service.GreetingService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //System.out.println( "Hello World!" );
    	
    	// -- Como acceder a un bean --
    	
    	// Iniciamos el contexto
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
    	
    	//usamos los beans
    	GreetingService beanService = null;
    	
    	// ID
    	//beanService = (GreetingService)appContext.getBean("greetingService");
    	
    	// ID + Class
    	beanService = appContext.getBean("greetingService", GreetingService.class);
    	
    	// Class .... solo si estas seguro que no puede regresar más que uno
    	//beanService = appContext.getBean(GreetingService.class);
    	
    	System.out.println(beanService.saludar());
    	
    	//cerramos contexto
    	((ClassPathXmlApplicationContext) appContext).close();
    	
    }
}
