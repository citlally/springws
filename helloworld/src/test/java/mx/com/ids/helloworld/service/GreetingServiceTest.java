package mx.com.ids.helloworld.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GreetingServiceTest {
	
	private GreetingService service;
	
	@Test
	public void testGreeting() {
		String param = "Hola mundo!";
		
		// Asigna valor a msg en GreetingService
		//service = new GreetingServiceImplement(param);
		// Recupera return de método saludar de GreetingServiceImplement
		String message = service.saludar();
		
		// Prueba que los la respuesta esperada y la obtenida sea la misma
		assertEquals(param, message);
	}

}
