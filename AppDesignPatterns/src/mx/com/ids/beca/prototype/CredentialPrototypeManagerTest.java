package mx.com.ids.beca.prototype;

import org.junit.Test;

public class CredentialPrototypeManagerTest {
	
	@Test
	public void testGetClonedCredential() throws Exception {
		PrototypeStudentCredential clonedHighSchoolCredential = CredentialPrototypeManager.getClonedCredential("hsc");
		clonedHighSchoolCredential.setStudentName("Ricardo Ramirez");
		clonedHighSchoolCredential.setGrade("segundo grado");
		System.out.println(clonedHighSchoolCredential);
		
		PrototypeStudentCredential clonedCollegeCredential = CredentialPrototypeManager.getClonedCredential("cc");
		clonedCollegeCredential.setStudentName("Miztly Lopez");
		clonedCollegeCredential.setGrade("Primer año");
		System.out.println(clonedCollegeCredential);
	}

}
