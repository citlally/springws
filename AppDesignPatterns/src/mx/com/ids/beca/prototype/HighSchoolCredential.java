package mx.com.ids.beca.prototype;

public class HighSchoolCredential extends PrototypeStudentCredential { // Prototype en concreto

	@Override
	public PrototypeStudentCredential cloneCredential() throws CloneNotSupportedException {
		HighSchoolCredential highSchoolCredential = null;
		
		try {
			highSchoolCredential = (HighSchoolCredential) super.clone(); // Crea clon de prototipo credencial
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return highSchoolCredential;
		
	}
	
	@Override
    public String toString() {
        return "[HighSchoolCredential: Student Name - " + getStudentName() 
        		+ ", Grade - " + getGrade() + "]";
    }
	
}
