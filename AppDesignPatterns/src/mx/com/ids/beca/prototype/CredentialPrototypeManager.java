package mx.com.ids.beca.prototype;

public class CredentialPrototypeManager {
	
	// Registro de los prototipos
	private static java.util.Map<String, PrototypeStudentCredential> prototypes = new java.util.HashMap<String, PrototypeStudentCredential>();
	
	// Block static para crear y agregar los prototipos a "map"
	static {
		HighSchoolCredential hsC = new HighSchoolCredential(); // Crea credenciales de tipo HichSchool
		hsC.setStudentName("Nombre del estudiante de preparatoria...");
		hsC.setGrade("Grado...");
		prototypes.put("hsc", hsC);
		
		CollegeCredential cC = new CollegeCredential(); // Crea credenciales de tipo College
		cC.setStudentName("Nombre del estudiante universitario...");
		cC.setGrade("Grado...");
		cC.setDegree("Licenciatura en Economia");
		prototypes.put("cc", cC);
	}
	
	public static PrototypeStudentCredential getClonedCredential(final String type) {
		PrototypeStudentCredential clonedCredential = null;
		
		try {
			PrototypeStudentCredential credential = prototypes.get(type); // obtiene el prototipo de un tipo de credencial especifico
			clonedCredential = credential.cloneCredential(); // Clona la credencial
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return clonedCredential;
	}

}
