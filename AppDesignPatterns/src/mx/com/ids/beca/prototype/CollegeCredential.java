package mx.com.ids.beca.prototype;

public class CollegeCredential extends PrototypeStudentCredential { // Prototype en concreto

	private String degree;
	
	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	@Override
	public PrototypeStudentCredential cloneCredential() throws CloneNotSupportedException {
		CollegeCredential collegeCredential = null;
		
		try {
			collegeCredential = (CollegeCredential) super.clone(); // Crea clon de prototipo credencial
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return collegeCredential;
		
	}
	
	@Override
    public String toString() {
        return "[CollegeCredential: Student Name - " + getStudentName() 
        		+ ", Grade - " + getGrade() + ", Degree - " + getDegree() + "]";
    }

}
