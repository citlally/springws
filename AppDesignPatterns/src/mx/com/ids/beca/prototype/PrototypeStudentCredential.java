package mx.com.ids.beca.prototype;

public abstract class PrototypeStudentCredential implements Cloneable {
	
	private String studentName;
	private String grade;
	
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	// Clase que a sobre escribir para craer clones de la credencial
	public abstract PrototypeStudentCredential cloneCredential() throws CloneNotSupportedException;

}
