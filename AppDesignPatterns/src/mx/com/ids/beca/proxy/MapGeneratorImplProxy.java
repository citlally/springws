package mx.com.ids.beca.proxy;

public class MapGeneratorImplProxy implements MapGenerator {
	MapGenerator mapGeneratorImpl;
	
	/*
	 * displayMapTemplate - la va a ejecutar el proxy, sin necesidad de llamar a
	 * la clase MapgeneratorImpl pues no es necesario cargar el resto ( que se supone
	 * será muy pesado ).
	 */
	@Override
	public void displayMapTemplate(String country, int ReferenceDots) {
		System.out.println("MapGeneratorImplProxy: se muestra template de mapa en blanco de " 
				+ country + " con " + ReferenceDots + " puntos de ubicación.");
	}
	
	/*
	 * generateMap - va a instanciar entonces MapGeneratorImpl
	 * para proceder a trabajar en el método generateMap.
	 * Esto pasará solo cuando es necesario hacerlo.
	 */
	@Override
	public void generateMap(String country, int ReferenceDots) {
		
		if(mapGeneratorImpl == null) {
			mapGeneratorImpl = new MapGeneratorImpl();
			mapGeneratorImpl.generateMap(country, ReferenceDots);
		}

	}

}
