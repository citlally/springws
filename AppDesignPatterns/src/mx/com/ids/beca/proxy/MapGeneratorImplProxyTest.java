package mx.com.ids.beca.proxy;

import org.junit.Test;

public class MapGeneratorImplProxyTest {
	
	@Test
	public void testGenerateMap() throws Exception {
		MapGenerator proxy = new MapGeneratorImplProxy();
		proxy.displayMapTemplate("México", 32);
		proxy.generateMap("México", 32);
	}

}
