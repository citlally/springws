package mx.com.ids.beca.proxy;

public class MapGeneratorImpl implements MapGenerator { // Clase Real

	public MapGeneratorImpl() {
		// Para mostrar cuando se crea la instancia 
		System.out.println("Instancia 'MapGeneratorImpl' creada");
	}
	
	/*
	 * Aquí van los métodos a implementar reales
	 * en donde se pretende podrian tener comunicación con BBDD,
	 * repositorios remotos que necesiten de procesar gran cantidad de datos
	 * y/o necesiten de una protección.
	 */
	
	@Override
	public void displayMapTemplate(String country, int ReferenceDots) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void generateMap(String country, int ReferenceDots) {
		System.out.println("MapGeneratorImpl: Generando mapa de: " + country
				+ " con " + ReferenceDots + " puntos de ubicación.");
	}

}
