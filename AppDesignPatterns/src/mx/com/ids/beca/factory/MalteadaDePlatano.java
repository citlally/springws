package mx.com.ids.beca.factory;

public class MalteadaDePlatano extends Malteada { // Clase ConcreteProduct que hereda de Product

	@Override
	public void agregaIngredientes() {
		System.out.println("Agregando ingredientes para la malteada de platano...");
	}

}
