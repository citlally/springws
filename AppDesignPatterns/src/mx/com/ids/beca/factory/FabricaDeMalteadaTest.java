package mx.com.ids.beca.factory;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class FabricaDeMalteadaTest {

	@Test
	public void testHacerMalteadas() {
		// Se crea objeto fabrica
		FabricaDeMalteadaBase fabricaDeMalteada = new FabricaDeMalteada();
		
		Malteada malteadaDePlatano = fabricaDeMalteada.hacerMalteada("Platano"); // Se fabrica malteada de platano
		Malteada malteadaDeFresa = fabricaDeMalteada.hacerMalteada("Fresa"); // Se fabrica malteada de fresa
		
		assertNotNull("El objeto es null", malteadaDePlatano);
		assertNotNull("El objeto es null", malteadaDeFresa);
		
		// --- Prueba para malteada que no esta en las opciones. ---
		//Malteada malteadaDeChocolate = fabricaDeMalteada.hacerMalteada("Chocolate"); // Se fabrica malteada de chocolate
		//assertNotNull("El objeto es null", malteadaDeChocolate);
	}
	
}
