package mx.com.ids.beca.factory;

public class MalteadaDeFresa extends Malteada { // Clase ConcreteProduct que hereda de Product

	@Override
	public void agregaIngredientes() {
		System.out.println("Agregando ingredientes para la malteada de fresa...");
	}

}
