package mx.com.ids.beca.factory;

public class FabricaDeMalteada extends FabricaDeMalteadaBase { // Clase ConcreatCreator que hereda de Creator

	@Override
	public Malteada hacerMalteada(String tipo) {
		Malteada malteada;
		
		switch(tipo.toLowerCase()) {
			
			case "fresa":
				malteada = new MalteadaDeFresa();
				break;
				
			case "platano":
				malteada = new MalteadaDePlatano();
				break;
			
			default: 
				System.out.println("No hay ese tipo de malteada.");
				malteada = null;
				break;
		
		}
		
		malteada.agregaIngredientes();
		malteada.bateMalteada();
		
		return malteada;
	}

}
