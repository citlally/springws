package mx.com.ids.beca.factory;

public abstract class FabricaDeMalteadaBase { // Clase asbtracta Creator
	
	// Declara el método factory que 
	//regresa un objeto tipo Product (en este caso Malteada)
	public abstract Malteada hacerMalteada(String tipo);

}
