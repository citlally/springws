package mx.com.ids.beca.factory;

public abstract class Malteada { // Clase abstracta "Producto"
	
	public abstract void agregaIngredientes(); // Método abstracto para @Override en los ConcreteProduct
	
	public void bateMalteada() {
		System.out.println("Batiendo la malteada...");
	}

}
