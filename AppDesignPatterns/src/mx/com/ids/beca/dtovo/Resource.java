package mx.com.ids.beca.dtovo;

import java.util.List;

public class Resource {
	
	private List<WorkerDto> workers;

	//Inicializa el resource con workers existentes,
	//Actuará como una BD
	public Resource(List<WorkerDto> workers) {
		this.workers = workers;
	}
	
	// regresa todos los workers en una lista
	public List<WorkerDto> getAllWorkers() {
		return workers;
	}
	
	//Nuevo worker
	public void save(WorkerDto worker) {
		workers.add(worker);
	}
	
	//Borrar worker
	public void delete(WorkerDto worker) {
		workers.remove(worker);
	}
	

}
