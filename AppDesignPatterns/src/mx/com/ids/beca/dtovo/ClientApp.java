package mx.com.ids.beca.dtovo;

import java.util.ArrayList;
import java.util.List;

public class ClientApp {

	public static void main(String[] args) {
		
		List<WorkerDto> workers = new ArrayList<>();
		WorkerDto workerOne = new WorkerDto("1", "Francisco", "Lopez");
		WorkerDto workerTwo = new WorkerDto("2", "Ricardo", "Hernandez");
		
		workers.add(workerOne);
		workers.add(workerTwo);
		
		Resource R = new Resource(workers);
		
		System.out.println("Lista de trabajadores: ");
		List<WorkerDto> allWorkers = R.getAllWorkers();
		printWorkersData(allWorkers);
		
		System.out.println("----------------------------------------------------------");
		System.out.println("------------ Eliminar trabajador con id 1 ----------------");
		System.out.println("----------------------------------------------------------");
		R.delete(workerOne);
		allWorkers = R.getAllWorkers();
		printWorkersData(allWorkers);

		System.out.println("----------------------------------------------------------");
		System.out.println("---------------- Agregar nuevo trabajador ----------------");
		System.out.println("----------------------------------------------------------");
		WorkerDto workerThree = new WorkerDto("3", "Carla", "Medina");
		R.save(workerThree);
		allWorkers = R.getAllWorkers();
		printWorkersData(allWorkers);

	}
	
	private static void printWorkersData(List<WorkerDto> allWorkers) {
		for(int i = 0; i < allWorkers.size(); i++) {
			System.out.println( allWorkers.get(i).getId() + " " 
								+ allWorkers.get(i).getFirstName() + " "
								+ allWorkers.get(i).getLastName());
		}
	}

}
