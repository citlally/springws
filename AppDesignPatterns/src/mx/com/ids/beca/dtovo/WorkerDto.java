package mx.com.ids.beca.dtovo;

public class WorkerDto {
	
	private final String id;
    private final String firstName;
    private final String lastName;
    
	public WorkerDto(String id, String firstName, String lastName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	

}
