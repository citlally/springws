package mx.com.ids.beca.injection.appinjection.impl;

import mx.com.ids.beca.injection.appinjection.interfaces.SpeakerService;
import mx.com.ids.beca.injection.appinjection.interfaces.TalkService;

public class TalkServiceImpl implements TalkService{
	
	// ---- CLASS PARA REFERENCIA DE BEANS ----
	
	private SpeakerService speakerService;
	
	public void speakWords(final String name) {
		System.out.println("Speaker: " + name);
		System.out.println("says: " + speakerService.speak() );
	}

	public void setSpeakerService(SpeakerService speakerService) {
		this.speakerService = speakerService;
	}
	
}
