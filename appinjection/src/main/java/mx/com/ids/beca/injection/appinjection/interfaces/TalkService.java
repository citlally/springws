package mx.com.ids.beca.injection.appinjection.interfaces;

public interface TalkService {
	
	public void speakWords(final String words);

}
