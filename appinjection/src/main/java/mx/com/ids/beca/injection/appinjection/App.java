package mx.com.ids.beca.injection.appinjection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import mx.com.ids.beca.injection.appinjection.interfaces.SpeakerService;
import mx.com.ids.beca.injection.appinjection.interfaces.TalkService;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // Accedemos al beans.xml
    	// Abrimos el contexto
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
    	
    	// Se usa el bean
    	SpeakerService beanService = null;
    	
    	
    	// ---------- METHOD INJECTION ----------
    	// Accedemos con ID + Class al primer bean
    	beanService = appContext.getBean("speakerServiceMethod", SpeakerService.class);
    	System.out.println(beanService.speak());
    	
    	// Accedemos con ID + Class al segundo bean
    	beanService = appContext.getBean("speakerServiceMethodSong", SpeakerService.class);
    	System.out.println(beanService.speak());
    	
    	// ---------- CONSTRUCTOR INJECTION ----------
    	// Accedemos con ID + Class al primer bean
    	beanService = appContext.getBean("speakerServiceConstructor", SpeakerService.class);
    	System.out.println(beanService.speak());
    	
    	// Accedemos con ID + Class al segundo bean
    	beanService = appContext.getBean("speakerServiceConstructorSong", SpeakerService.class);
    	System.out.println(beanService.speak());
    	
    	// ---------- REFERENCIA DE BEANS ----------
    	// Accedemos por Class
    	TalkService talkService = appContext.getBean(TalkService.class);
    	talkService.speakWords("Ramón"); // Le pasamos el parametro
    	
    	
    	// Se cierra el contexto
    	((ClassPathXmlApplicationContext) appContext).close();
    }
}
