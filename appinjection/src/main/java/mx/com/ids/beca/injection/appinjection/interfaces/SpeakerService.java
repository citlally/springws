package mx.com.ids.beca.injection.appinjection.interfaces;

public interface SpeakerService {
	String speak();
}
