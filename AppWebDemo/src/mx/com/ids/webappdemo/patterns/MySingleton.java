package mx.com.ids.webappdemo.patterns;

public class MySingleton {
	
	private static MySingleton instance = null;
	private static final String MESSAGE = "Yea yea !!!";
	private static final Integer ID = 1;
	
	private MySingleton() {}
	
	public static MySingleton getinstance() {
		if (instance == null) {
			instance = new MySingleton();
		}
		return instance;
	}
	
	public static String getMessage() {
		return MESSAGE;
	}

	@Override
	public String toString() {
		return super.toString() + " ID: " + ID;
	}
	
}
