package mx.com.ids.webappdemo.patterns;

public class TestMySingleton {
	
	public static void main(String[] args) {
		System.out.println("Test mysingleton");
		
		//MySingleton mySingletonInstance = new MySingleton();
		MySingleton mySingletonInstance = MySingleton.getinstance();
		
		System.out.println("Mensaje : " + MySingleton.getMessage() +
				"ID : " + mySingletonInstance);
	}

}
